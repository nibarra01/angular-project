import {Component, OnInit, ViewChild} from '@angular/core';
import {Album} from "../../models/Album";

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {
  albums: Album[];
  newAlbum: Album = {
    title:  '',
    artist: '',
    songs:  [],
    year: null,
    genre:  '',
  };
  newAlbumUnits: null;
  albumFormSwitch: boolean;
  songlist: any;
  songlistvar1: any;
  songlistvar2: any;
  @ViewChild('albumform')form: any;

  constructor() { }

  ngOnInit(): void {

    // ======================================== Albums Array
    this.albums = [
    //------------------------------------------- Album 1
      {
        title:  'Drunken Lullabies',
        artist: 'Flogging Molly',
        songs:  [
          'Drunken Lullabies',
          "What's Left of the Flag",
          'May the Living Be Dead (In Our Wake)',
          'The Killburn High Road',
          'Rebels of the Sacred Heart',
          'Swagger',
          'Cruel Mistress',
          'Death Valley Queen',
          'Another Bag of Bricks',
          'The Rare Ould Times',
          'The Son Never Shines'
        ],
        year: 2002,
        genre: 'Celtic punk',
        // do images only work if they're under the assets dir?
        cover:  '../../assets/img/drunkenlullabies.jpg'
      },
  //  -------------------------------------------- /Album 1
  //    ---------------------------------------------- Album 2
      {
        title: 'Rise and Fall, Rage and Grace',
        artist: 'The Offspring',
        songs:  [
          "Half-Trusim",
          'Trust in You',
          "You're Gonna Go Far, Kid",
          'Hammerhead',
          'A Lot Like Me',
          'Takes me Nowhere',
          "Kristy, Are You Doing Okay?",
          'Nothingtown',
          'Stuff Is Messed Up',
          'Fix You',
          "Let's Hear It for Rock Bottom",
          'Rise and Fall'
        ],
        year:2008,
        genre: 'Punk rock',
        units:554000,
        cover:"../assets/img/riseandfallrageandgrace.jpg"
      },
    //  ------------------------------------------- /Album 2
    //  -------------------------------------------- Album 3
      {
        title: 'Factory Showroom',
        artist: 'They Might Be Giants',
        songs: [
          'Token Back to Brooklyn',
          'S-E-X-X-Y',
          'Till My Head Falls Off',
          'How Can I Sing Like a Girl?',
          'Exquisite Dead Guy',
          'Metal Detector',
          'New York City',
          'Your Own Worst Enemy',
          'XTC vs. Adam Ant',
          'Spirialing Shape',
          'James K. Polk',
          'Pet Name',
          'I Can Hear You',
          'The Bells Are Ringing'
        ],
        year:1996,
        genre:'Alternative Rock',
        cover: '../../assets/img/factoryshowroom.jpg'
      },
    //  -------------------------------------- /Album 3
    //  ------------------------------------------ Album 4
      {
        title: 'London Calling',
        artist: 'The Clash',
        songs: [
          'Hateful',
          "Rudie Can't Fail",
          "Paul's Tune",
          "I'm Not Down",
          '4 Horsemen',
          'Koka Kola, Advertising & Cocaine',
          'Death or Glory',
          "Lover's Rock",
          'Lonesome Me',
          'The Police Walked in 4 Jazz',
          'Lost in the Supermarket',
          'Up-Toon',
          'Walking the Sidewalk',
          'Where You Gonna Go (Soweto)',
          'The Man in Me',
          'Remote Control',
          'Working and Waiting',
          'Heart & Mind',
          'Brand New Cadillac',
          'London Calling',
          'Revolution Rock'
        ],
        year: 1979,
        genre: 'Post-punk, Punk rock',
        units: 1823888,
        cover: '../../assets/img/londoncalling.jpg'
      },
    //  -------------------------------------------- /Album 4
    //  ----------------------------------------- Album 5
      {
        title: 'Hot Fuss',
        artist: 'The Killers',
        songs: [
          'Jenny Was a Friend of Mine',
          'Mr. Brightside',
          'Smile Like You Mean It',
          'Somebody Told Me',
          "All These Things That I've Done",
          "Andy, You're a Star",
          'On Top',
          'Change Your Mind',
          'Believe Me Natalie',
          'Midnight Show',
          'Everything Will Be Alright',
          'Glamorous Indie Rock & Roll'
        ],
        year: 2004,
        genre:'Alternative rock, Post-punk revival, Indie rock, New Wave, Synth-pop',
        units:6688495,
        cover: "../..//assets/img/hotfuss.jpg"
      },
    //  --------------------------------------------- /Album 5
    ]
  //  ========================================== /Albums Array


    this.albumFormSwitch = false;
    this.songlistvar2 = 1;
    // console.log(this.songlist);

    // setInterval(i => {
    //   console.log()
    // }, 2000)

  }

  addAlbum(e){


    this.newAlbum.songs = this.songlist
    this.albums.push(this.newAlbum);

    this.newAlbum = {
      title:  '',
        artist: '',
        songs:  [],
        year: null,
        genre:  '',
      units:null,
    };

    this.songlist = []
    this.songlistvar2 = 1
  }

  toggle(e){
    this.albumFormSwitch = !this.albumFormSwitch
    if (this.songlist === undefined){
      this.songlistvar1 = [1];
      this.songlist = [''];
    }
  }

  songUp(e){
    if (this.songlistvar2 <= 32){
      this.songlistvar2 += 1

      this.songlistvar1.push(this.songlistvar2);
    } else {
      alert("If your album has more than 33 songs, contact an administrator.")
    }

    // console.log(this.songlistvar1)
    // console.log(this.songlist)
  }

  songDown(e){
    if (this.songlistvar2 >= 2){
      this.songlistvar2 -= 1
      this.songlistvar1.pop();
    } else {
      alert("something happened.");
    }

    // console.log(this.songlistvar1)
  }
  onSubmit({value, valid}: {value: Album, valid: boolean}) {

    if (!valid) {
      // console.log('Form is not valid')
      // alert("Form is not valid.")
    } else {
      // console.log(value);
    }



  }



}
