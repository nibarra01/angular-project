export interface Album{
  title:  string,
  artist: string,
  songs:  string[],
  year: number,
  genre:  string,
  units?:  number,
  cover?: string,
}
